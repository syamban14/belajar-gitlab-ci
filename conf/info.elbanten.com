server {
    listen 80;
    server_name info.elbante.com www.info.elbanten.com;

    root /usr/share/nginx/html;
    index index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }
}
