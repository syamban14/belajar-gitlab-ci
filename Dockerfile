FROM nginx
COPY ./html/. /usr/share/nginx/html/.
COPY ./conf/info.elbanten.com /etc/nginx/sites-available/info.elbanten.com
COPY ./conf/info.elbanten.com /etc/nginx/sites-enabled/info.elbanten.com
COPY ./conf/nginx.conf /etc/nginx/nginx.conf
